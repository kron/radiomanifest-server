from django.core.management.base import BaseCommand, CommandError
from rest_framework_api_key.models import APIKey


class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def add_arguments(self, parser):
        parser.add_argument('name', type=str,
                            help='Name of the API Key')

    def handle(self, *args, **options):
        api_key, key = APIKey.objects.create_key(name=options['name'])
        message = (
            "The API key for {} is: {}. ".format(api_key.name, key) 
            + "\nPlease store it somewhere safe: "
            + "\nyou will not be able to see it again."
        )
        print(message)
