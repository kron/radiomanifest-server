from django.urls import path, include

urlpatterns = [
    path('', include('core.radiomanifest_api.urls')),
    path('api/', include('core.dev_api.urls')),
]