
from django.contrib import admin
from django.forms import models
from core.models import (
    PodcastItem, Schedule, Show, StreamingSource, Enclosure, RadioStation, StremingSourceItem)


class StremingSourceItemInline(admin.TabularInline):
    model = StremingSourceItem


class StremingSourceInline(admin.TabularInline):
    model = StreamingSource


class EnclosureInline(admin.TabularInline):
    model = Enclosure
    readonly_fields = ['url', 'length', 'type']

    def has_delete_permission(self, request, obj=None):
        return False


class ShowInline(admin.TabularInline):
    model = Show


class PodcastItemInline(admin.TabularInline):
    model = PodcastItem

@admin.register(Show)
class ShowAdmin(admin.ModelAdmin):
    model = Show
    list_display = ['name', 'radio_station']

@admin.register(StreamingSource)
class StreamingSourceAdmin(admin.ModelAdmin):
    model = StreamingSource
    inlines = [
        StremingSourceItemInline,
    ]


@admin.register(RadioStation)
class RadioStationAdmin(admin.ModelAdmin):
    model = RadioStation
    list_display = ['name', 'is_active']
    inlines = [
        StremingSourceInline
    ]


@admin.register(PodcastItem)
class PodcastItemAdmin(admin.ModelAdmin):
    model = PodcastItem
    list_display = ['title', 'show', 'publicated_at', 'duration']

    readonly_fields = ['title', 'link', 'description', 'publicated_at',
                       'duration', 'show', 'updated_at', 'guid', 'guid_is_permalink']
    inlines = [
        EnclosureInline,
    ]


@admin.register(Schedule)
class ScheduleAdmin(admin.ModelAdmin):
    model = Schedule
    list_display = ['name', 'start_at', 'end_at', 'radio_station', 'show']
    readonly_fields = ['name', 'description', 'start_at', 'end_at']
