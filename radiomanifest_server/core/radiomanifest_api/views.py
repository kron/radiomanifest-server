
from cgitb import lookup
from django.views.generic import TemplateView, ListView, DetailView
from core.models import Show, StreamingSource, RadioStation, RADIO_STATION_ID
from django.http import JsonResponse
from django.http import Http404


class ManifestDetailView(DetailView):
    model = RadioStation
    template_name = "radio_manifest/radio_manifest.xml"
    content_type = 'application/xml'

    def get_object(self, queryset=None):
        return RadioStation.objects.get(id=RADIO_STATION_ID)


class ShowListView(ListView):
    model = Show
    template_name = "radio_manifest/shows.xml"
    content_type = 'application/xml'

    def get_queryset(self):
        try:
            radio_station = RadioStation.objects.get(id=RADIO_STATION_ID)
            return radio_station.shows.all()
        except RadioStation.DoesNotExist:
            return Show.objects.none()


class StreamingSourceDetailView(DetailView):
    model = StreamingSource
    template_name = "radio_manifest/streaming_source.m3u"
    lookup = 'slug'
    content_type = 'audio/x-mpegurl '

    def get_object(self, queryset=None):
        slug = self.kwargs['slug']

        if slug:
            radio_station = RadioStation.objects.get(id=RADIO_STATION_ID)
            return StreamingSource.objects.get(radio_station=radio_station, slug=slug)


class StreamingInfoDetailView(DetailView):
    model = RadioStation
    lookup = 'slug'

    def get_object(self, queryset=None):
        return RadioStation.objects.get(id=RADIO_STATION_ID)

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        try:
            primary_streaming_sources = self.object.streaming_sources.order_by(
                '-priority').first()
            primary_stream_url = primary_streaming_sources.items.first().url
        except:
            primary_stream_url = None

        return JsonResponse({
            "icy-index-metadata": 1,
            "icy-version": 2,
            "icy-name": self.object.name,
            "icy-description": self.object.description,
            "icy-main-stream-url": primary_stream_url,
            "icy-language-codes": "ita",
            "icy-country-code": "IT",
            "icy-country-subdivison-code": "IT-GE",
            "icy-logo": self.object.image.url
        })

    