from django.urls import path

from .rss_feeds import RadioFeed, ShowFeed
from .ics_feeds import RadioSchedulesFeed, ShowSchedulesFeed
from .views import ManifestDetailView, StreamingInfoDetailView, ShowListView, StreamingSourceDetailView

urlpatterns = [
    path('radiomanifest.xml', ManifestDetailView.as_view()),
    path('streaminfo.json', StreamingInfoDetailView.as_view()),

    path('schedules.ics', RadioSchedulesFeed()),
    path('feed.xml', RadioFeed()),
    path('shows.xml', ShowListView.as_view()),

    path('shows/<slug:show_slug>/feed.xml', ShowFeed()),
    path('shows/<slug:show_slug>/schedules.ics', ShowSchedulesFeed()),
    path('streaming-sources/<slug:slug>.m3u',
         StreamingSourceDetailView.as_view()),
]
