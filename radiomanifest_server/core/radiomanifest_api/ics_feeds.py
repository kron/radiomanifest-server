import datetime

import django_cal
from django_cal.views import Events
import dateutil.rrule as rrule
from core.models import RadioStation, Schedule, Show, RADIO_STATION_ID
import pytz

# utc = pytz.utc.localize()
class RadioSchedulesFeed(Events):
    
    def get_object(self, request):
        return RadioStation.objects.get(id=RADIO_STATION_ID)
       
    def items(self, obj):
        return Schedule.objects.filter(radio_station=obj)

    def item_summary(self, item):
        return item.name

    def item_description(self, item):
        return item.description

    def item_start(self, item):
        return item.start_at.astimezone(pytz.utc)

    def item_end(self, item):
        return item.end_at.astimezone(pytz.utc)

    def item_categories(self, item):
        return item.show.name if item.show else None

class ShowSchedulesFeed(RadioSchedulesFeed):
    
    def get_object(self, request, show_slug):
        radio_station = RadioStation.objects.get(id=RADIO_STATION_ID)
        return Show.objects.get(radio_station=radio_station, slug=show_slug)

    def cal_name(self, obj):
        return obj.name
    
    def cal_desc(self, obj):
        return obj.description
    
    def items(self, obj):
        return Schedule.objects.filter(show=obj)


    