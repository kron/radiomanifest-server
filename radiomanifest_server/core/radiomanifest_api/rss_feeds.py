from django.contrib.syndication.views import Feed
from django.urls import reverse
from core.models import PodcastItem, Show, RadioStation, RADIO_STATION_ID
from django.utils.feedgenerator import Rss201rev2Feed


class PodcastFeed(Rss201rev2Feed):
    def root_attributes(self):
        attrs = super().root_attributes()
        attrs['xmlns:itunes'] = 'http://www.itunes.com/dtds/podcast-1.0.dtd'
        return attrs

    def add_root_elements(self, handler):
        super().add_root_elements(handler)
        if self.feed['itunes:image'] is not None:
            handler.addQuickElement("itunes:image", self.feed["itunes:image"])

    def add_item_elements(self, handler, item):
        super().add_item_elements(handler, item)
        if item['description'] is not None:
            handler.addQuickElement("itunes:summary", item["description"])

        if item['itunes:author'] is not None:
            handler.addQuickElement("itunes:author", item["itunes:author"])

        if item['itunes:duration'] is not None:
            handler.addQuickElement("itunes:duration", item["itunes:duration"])


class RadioFeed(Feed):
    feed_type = PodcastFeed
    
    def get_object(self, request):
        return RadioStation.objects.get(id=RADIO_STATION_ID)

    def title(self, obj):
        return obj.name

    def link(self, obj):
        return obj.website

    def description(self, obj):
        return obj.description

    def feed_extra_kwargs(self, obj):
        return {
            'itunes:image': obj.image.url if obj.image else None,
        }

    def items(self, obj):
        return PodcastItem.objects.filter(show__radio_station=obj).order_by('-publicated_at')

    def item_title(self, item):
        return item.title

    def item_description(self, item):
        return item.description

    # item_link is only needed if NewsItem has no get_absolute_url method.
    def item_link(self, item):
        return item.show.website

    def item_guid(self, item):
        return item.guid

    def item_guid_is_permalink(self, item):
        return item.guid_is_permalink

    def item_enclosure_url(self, item):
        return item.enclosure.url

    def item_enclosure_length(self, item):
        return item.enclosure.length

    def item_enclosure_mime_type(self, item):
        return item.enclosure.type

    def item_extra_kwargs(self, item):
        return {
            'itunes:author': item.show.name,
            'itunes:duration': str(item.duration),
        }


class ShowFeed(RadioFeed):

    def get_object(self, request, show_slug):
        radio_station = RadioStation.objects.get(id=RADIO_STATION_ID)
        return Show.objects.get(radio_station=radio_station, slug=show_slug)

    def title(self, obj):
        return obj.name

    def link(self, obj):
        return obj.website

    def description(self, obj):
        return obj.description

    def feed_extra_kwargs(self, obj):
        return {
            'itunes:image': obj.image.url if obj.image else None,
        }

    def items(self, obj):
        return PodcastItem.objects.filter(show=obj).order_by('-publicated_at')
