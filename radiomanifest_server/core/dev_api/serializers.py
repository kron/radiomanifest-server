from rest_framework import serializers
from core.models import (
    PodcastItem, Schedule, Show, StreamingSource, Enclosure, RadioStation, StremingSourceItem)


class RadioStationSerializer(serializers.ModelSerializer):
    class Meta:
        model = RadioStation
        fields = '__all__'


class ShowSerializer(serializers.ModelSerializer):
    class Meta:
        model = Show
        exclude = ["radio_station"]


class ScheduleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Schedule
        exclude = ["radio_station"]


class PodcastItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = PodcastItem
        fields = '__all__'

class EnclosureSerializer(serializers.ModelSerializer):
    class Meta:
        model = Enclosure
        fields = '__all__'

class StreamingSourceSerializer(serializers.ModelSerializer):
    class Meta:
        model = StreamingSource
        fields = '__all__'

class StremingSourceItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = StremingSourceItem
        fields = '__all__'
