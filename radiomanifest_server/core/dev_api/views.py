from rest_framework import viewsets, permissions, mixins, generics, views, renderers
from rest_framework.response import Response

from core.models import (
    PodcastItem, Schedule, Show, StreamingSource,
    Enclosure, RadioStation, StremingSourceItem)

from core.models import RADIO_STATION_ID
from rest_framework_api_key.permissions import HasAPIKey
from django.views.generic.base import TemplateView


from core.dev_api.serializers import (
    PodcastItemSerializer, ScheduleSerializer, ShowSerializer,
    StreamingSourceSerializer, EnclosureSerializer,
    RadioStationSerializer, StremingSourceItemSerializer)


class RadioStationRetrieveUpdateAPIView(generics.RetrieveUpdateAPIView):
    queryset = RadioStation.objects.all()
    serializer_class = RadioStationSerializer

    def get_object(self):
        return self.queryset.get(id=RADIO_STATION_ID)


class PodcastItemViewSet(viewsets.ModelViewSet):
    queryset = PodcastItem.objects.all()
    serializer_class = PodcastItemSerializer


class ScheduleViewSet(viewsets.ModelViewSet):
    queryset = Schedule.objects.all()
    serializer_class = ScheduleSerializer


class ShowViewSet(viewsets.ModelViewSet):
    queryset = Show.objects.all()
    serializer_class = ShowSerializer


class StreamingSourceViewSet(viewsets.ModelViewSet):
    queryset = StreamingSource.objects.all()
    serializer_class = StreamingSourceSerializer


class EnclosureViewSet(viewsets.ModelViewSet):
    queryset = Enclosure.objects.all()
    serializer_class = EnclosureSerializer


class StremingSourceItemViewSet(viewsets.ModelViewSet):
    queryset = StremingSourceItem.objects.all()
    serializer_class = StremingSourceItemSerializer

# TOFIX
class OpenAPISchema(TemplateView):
    template_name = 'openapi-schema.yml'
    content_type = 'application/yaml'

