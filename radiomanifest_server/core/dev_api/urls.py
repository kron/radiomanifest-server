from rest_framework import routers
from core.dev_api.views import (
    PodcastItemViewSet, ScheduleViewSet, ShowViewSet,
    StreamingSourceViewSet, EnclosureViewSet, RadioStationRetrieveUpdateAPIView,
    StremingSourceItemViewSet, OpenAPISchema)
from django.urls import path

router = routers.SimpleRouter()

router.register(r'shows', ShowViewSet)
router.register(r'schedules', ScheduleViewSet)
router.register(r'podcast-items', PodcastItemViewSet)
router.register(r'enclosures', EnclosureViewSet)
router.register(r'streaming-sources', StreamingSourceViewSet)
router.register(r'streaming-sources-items', StremingSourceItemViewSet)


urlpatterns = [
    path('radio-station/', RadioStationRetrieveUpdateAPIView.as_view(),
         name='radio-station'),

    path('', OpenAPISchema.as_view(),
         name='openapi-schema')
]

urlpatterns += router.urls
