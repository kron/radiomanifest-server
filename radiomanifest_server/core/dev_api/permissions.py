from rest_framework_api_key.permissions import HasAPIKey


class HasAPIKeyUrl(HasAPIKey):
    def get_key(self, request):
        return self.key_parser.get(request)
