from django.db import models
from django.utils.text import slugify
import pathlib
from django.db import transaction
from django.conf import settings

RADIO_STATION_ID = settings.RADIO_STATION_ID


def show_image_upload_path(instance, filename):
    extension = pathlib.PurePosixPath(filename).suffix
    return f'radiomanifest/shows/{instance.name}_image{extension}'


def radio_station_upload_path(instance, filename):
    extension = pathlib.PurePosixPath(filename).suffix
    return f'radiomanifest/{instance.name}{extension}'


class StreamingSource(models.Model):
    name = models.CharField(max_length=256)
    slug = models.SlugField(blank=True)
    priority = models.IntegerField(default=1)
    radio_station = models.ForeignKey('core.RadioStation',
                                      on_delete=models.CASCADE,
                                      blank=True, default=RADIO_STATION_ID,
                                      related_name='streaming_sources')

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.name)
        super(StreamingSource, self).save(*args, **kwargs)

    def __str__(self):
        return self.name


class StremingSourceItem(models.Model):
    url = models.URLField()
    streaming_source = models.ForeignKey('core.StreamingSource',
                                         on_delete=models.CASCADE,
                                         related_name='items')


class Show(models.Model):
    name = models.CharField(max_length=256)
    website = models.URLField()
    description = models.TextField()
    slug = models.SlugField(blank=True)
    image = models.ImageField(null=True, blank=True,
                              upload_to=show_image_upload_path)
    radio_station = models.ForeignKey('core.RadioStation',
                                      on_delete=models.CASCADE,
                                      blank=True, default=RADIO_STATION_ID,
                                      related_name='shows')

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.name)
        if not self.image:
            self.image = self.radio_station.image
        super(Show, self).save(*args, **kwargs)

    def __str__(self):
        return self.name


class Schedule(models.Model):
    name = models.CharField(max_length=256)
    description = models.TextField(null=True, blank=True)
    end_at = models.DateTimeField()
    start_at = models.DateTimeField()
    radio_station = models.ForeignKey('core.RadioStation',
                                      on_delete=models.CASCADE,
                                      related_name='schedules')
    show = models.ForeignKey('core.Show',
                             on_delete=models.CASCADE,
                             related_name='schedules',
                             blank=True, null=True)

    def __str__(self):
        return self.name


class Enclosure(models.Model):

    class MIMEType(models.TextChoices):
        MP3 = "audio/mp3", 'Mp3 File'

    url = models.URLField()
    length = models.IntegerField()
    type = models.CharField(max_length=128, choices=MIMEType.choices)
    podcast_item = models.OneToOneField('core.PodcastItem',
                                        on_delete=models.CASCADE,
                                        related_name='enclosure')


class PodcastItem(models.Model):
    title = models.CharField(max_length=128, null=True, blank=True)
    link = models.URLField()
    description = models.TextField(null=True, blank=True)
    publicated_at = models.DateTimeField()
    updated_at = models.DateTimeField(auto_now=True)
    guid = models.CharField(max_length=128, unique=True, editable=False)
    guid_is_permalink = models.BooleanField(default=False)
    duration = models.DurationField(blank=True, null=True, default=None)
    show = models.ForeignKey('core.Show',
                             on_delete=models.CASCADE,
                             related_name='items')

    class Meta:
        ordering = ['-publicated_at']

    def __str__(self):
        return self.title


class RadioStation(models.Model):
    name = models.CharField(max_length=128)
    slug = models.SlugField(blank=True)
    website = models.URLField()
    description = models.TextField(blank=True)
    image = models.ImageField(blank=True, null=True,
                              upload_to=radio_station_upload_path)

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.name)

    def __str__(self):
        return self.name

    @property
    def is_active(self):
        return self.id == RADIO_STATION_ID
