#!/bin/sh

if [[ $DEBUG == 1 ]]; then
    ./manage.py runserver 0.0.0.0:8000
else
    gunicorn radiomanifest_server.wsgi -b :8000
fi