# django-radio-manifest
Server-side implementation of radiomanifest, a standard for structurally displaying information about a webradio.
Radiomanifest specification https://radiomanifest.degenerazione.xyz/v0.2/spec.html

### Run
`sudo docker-compose up --build`

### Developer API 
To manage your data manually, you can use the django admin interface.
In most real use cases though, the manifest is integrated into a pre-existing ecosystem so what you would need is to be able to automate updates.
To do this the application exposes a developer API that allows you to modify any data in your manifest from any HTTP client.

#### API Key
To access the API you must have an API key, you can generate your keys by running: `./manage.py create_api_key <name_of_the_key>`.
Clients must pass their API key via the Authorization header. It must be formatted as follows: `Authorization: Api-Key <API_KEY>`
